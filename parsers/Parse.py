#!/usr/bin/python
import sys
import os
import time
import threading
import subprocess
from urllib2 import *
import BeautifulSoup
import subprocess
from os.path import expanduser

home = expanduser("~")

Name=sys.argv[1]
Path=sys.argv[2]

def Code(z):
   a=z[::-1]
   ans=[]
   ans.append('')
   ans.append('')
   flag=0
   for i in a:
      if(i=='/'):
         ans[1] = ans[1][::-1]
         return ans
      if(flag==1 and i!='_'):
         ans[0]+=i
      if(flag==2):
         ans[1]+=i
      if(i=='_'):
         flag=2
      if(i=='.'):
         flag=1

print "Name : ", Name
print "Path : ", Path
f = open('/tmp/Path', 'w')
f.write(Path)
f.close()
PC, CC = Code(Path + '/' + Name)
print PC, CC
#-------------------------------------------

Url='http://codeforces.com/contest/'+CC+'/problem/'+PC
try:
   html=urlopen(Url,timeout=10000).read()
   html=html.replace('\r','')
   soup=BeautifulSoup.BeautifulSoup(html)
except Exception:
   pass
try:
   PRE=soup.findAll('pre')
except Exception:
   print " ********   Network Error, Can not fetch test cases  ******** "
   exit(0)

L=len(PRE)
TestCount = 1

for i in xrange(L):
   if(i%2==0):
      In=str(PRE[i])
      In=In.replace('<pre>','').replace('</pre>','')
      In=In.replace('&gt;','>')
      In=In.replace('&lt;','<')
      In=In.replace('&quot;','"')
      In=In.replace('&amp;','&')
      In=In.replace('<br />','\n')
      In=In.replace('<br/>','\n')
      In=In.replace('</ br>','\n')
      In=In.replace('</br>','\n')
      In=In.replace('<br>','\n')
      In=In.replace('< br>','\n')
      In=In.split('\n')
      f=open(Path + '/' + str(TestCount) + '.in','w')
      for j in In:
         y=j.replace('\n','')
         if(y!='' and y!=' '):
            f.write(y+'\n')
      f.close()
   else:
      Out=str(PRE[i])
      Out=Out.replace('<pre>','').replace('</pre>','')
      Out=Out.replace('&gt;','>')
      Out=Out.replace('&lt;','<')
      Out=Out.replace('&quot;','"')
      Out=Out.replace('&amp;','&')
      Out=Out.replace('<br />','\n')
      Out=Out.replace('<br/>','\n')
      Out=Out.replace('</ br>','\n')
      Out=Out.replace('<br>','\n')
      Out=Out.split('\n')
      f=open(Path + '/' + str(TestCount) + '.out','w')      
      for j in Out:
         y=j.replace('\n','')
         if(y!='' and y!=' '):
            f.write(y+'\n')
      f.close()
      TestCount += 1
