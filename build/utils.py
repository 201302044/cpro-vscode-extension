#!/usr/bin/python

import sys
import os
import time
import threading
import subprocess
import re
import config

PRINTLIMIT = config.PRINT_LIMIT

home = os.path.expanduser("~")
onlyString = False

class bcolors:
    HEADER = ''#'\033[95m'
    OKBLUE = ''#'\033[94m'
    OKGREEN = ''#'\033[92m'
    WARNING = ''#'\033[93m'
    FAIL = ''#'\033[91m'
    ENDC = ''#'\033[0m'
    BOLD = ''#'\033[1m'
    UNDERLINE = ''#'\033[4m'

def read(fname):
   f = open(fname)
   res = f.read()
   f.close()
   return res

def strip_non_ascii(s):
	stripped = (c for c in s if 0 < ord(c) < 127)
	return ''.join(stripped)

def strClean(s, forTopCoder = False):
	s = strip_non_ascii(s)
	if(forTopCoder):
		s = s.replace('\t', ' ')
		s = re.sub(' +',' ',s)
		s = s.strip('\n')
		s = s.strip()
		return s
	else:
		s = s.strip()
		return s + '\n'

def truncate(s):
	k = ''
	t = s[:PRINTLIMIT]
	while(len(t) > 0 and t[-1] == ' '):
		t = t[:-1]
	if(len(s) >= PRINTLIMIT + 1):
		k += '... [truncated]'
	return strClean(t + k)

class Command(object):
	def __init__(self, cmd, inp):
		self.cmd = cmd.split(' ')
		self.process = None
		self.inp = inp

	def run(self, timeout):
		def target():
			if(self.inp != ''):
				myInput = open(self.inp, 'r')
			else:
				myInput = open(home + '/tools/tmp/nothing.txt', 'r')
			myOutput = open(home + '/tools/tmp/tout','w')
			self.process = subprocess.Popen(self.cmd, bufsize = 4096, shell = False, stdin = myInput, stdout = myOutput)
			self.process.communicate()
			myOutput.flush()
		return_code = 0
		thread = threading.Thread(target = target)
		thread.start()
		thread.join(timeout)
		if thread.isAlive():
			return_code = 123456
			try:
				self.process.terminate()
	 			thread.join()
	 		except AttributeError:
		 		pass
		else:
		 	return_code = self.process.returncode
		return return_code

def cleanRTE(inputPath, pathToExex, isTopcoder = False, testCase = None):
	if(isTopcoder):
		command = "gdb -q -x " + home + "/tools/tmp/gdbinTopCoder " + pathToExex + " > " + home + "/tools/tmp/gdbout"
		#print command
		#exit(0)
		f = open(home + '/tools/tmp/gdbinTopCoder', 'w')
		f.write('python sys.path.append("/usr/share/gcc-4.8/python");\n')
		f.write('r ' + str(testCase) + '\n')
		f.write('q\n')
		f.close()
	else:
		os.system('cp "' + inputPath + '" ' + home + '/tools/tmp/tin')
		command = "gdb -q -x " + home + "/tools/tmp/gdbin " + pathToExex + " > " + home + "/tools/tmp/gdbout"
	os.system(command)
	f = open(home + '/tools/tmp/gdbout')
	text = f.read()
	text = text.replace('Reading symbols from ' + '%s...done.' % pathToExex, '')
	text = text.replace('A debugging session is active.', '')
	text = text.replace('Quit anyway? (y or n) [answered Y; input not from terminal]', '')
	f.close()
	ttext = text.split('\n')
	res = ''
	for i in ttext:
		if(str(i) == ''  or 'Inferior' in str(i) or 'Input' in str(i) or 'Expected' in str(i) or 'Testcase #' in str(i)):
			continue
		else:
			res += str(i)
			res += '\n'
	return res

def checkNumber(x, y, epsilon): #Taken from google-codejam.
  if -epsilon <= x - y <= epsilon:
    return True

  if -epsilon <= x <= epsilon or -epsilon <= y <= epsilon:
    return False

  return (-epsilon <= (x - y) / x <= epsilon
       or -epsilon <= (x - y) / y <= epsilon)

def checkResult(expectedPath):
	receivedPath = home + '/tools/tmp/tout'
	f = open(receivedPath)
	x = strClean(f.read())
	out = x
	f.close()
	f = open(expectedPath)
	y = strClean(f.read())
	expOut = y
	f.close()
	tx = x.split('\n')
	ty = y.split('\n')
	x = []
	y = []
	for i in tx:
		j = i
		j = j.strip(' ').strip().strip(' ')
		if(j == ''):
			continue
		else:
			x.append(j)
	for i in ty:
		j = i
		j = j.strip(' ').strip().strip(' ')
		if(j == ''):
			continue
		else:
			y.append(j)
	if((len(y) == 0) or (len(y) == 1 and y[0].lower() == 'null') or (len(y) == 1 and y[0].lower() == 'none')):
		return out, 'Unknown', 'Unknown'
	if(len(x) != len(y)):
		if(len(x) > len(y)):
			return out, 'PE', 'Only ' + str(len(y)) + ' lines expected'
		elif(len(x) < len(y)):
			return out, 'PE', 'More than ' + str(len(x)) + ' lines expected'
	else:
		L = len(x)
		for i in xrange(L):
			p = x[i]
			q = y[i]
			p = p.split(' ')
			q = q.split(' ')
			l1 = len(p)
			l2 = len(q)
			if(l1 != l2):
				if(l1 > l2):
					return out, 'PE', 'Only ' + str(l2) + ' tokens expected in line #' + str(i + 1)
				elif(l1 < l2):
					return out, 'PE', 'More than ' + str(l1) + ' tokens expected in line #' + str(i + 1)
			for j in xrange(l1):
				c1 = p[j]
				c2 = q[j]
				if(onlyString): #onlyString TODO add to conf
					if(c1 != c2):
						return out, 'WA', 'Difference in line #' + str(i + 1) + ', token #' + str(j + 1)
					continue
				try:
					c1 = float(c1)
					c2 = float(c2)
					if(not checkNumber(c1, c2, 1e-6)):
						return out, 'WA', 'Difference in line #' + str(i + 1) + ', token #' + str(j + 1)
				except Exception:
					if(c1 != c2):
						return out, 'WA', 'Difference in line #' + str(i + 1) + ', token #' + str(j + 1)
		return out, 'OK', ''

def getTestDataTopcoder(x, failed = False):
	if(failed):
		pattern = re.compile("Input: (.*)\n(.*)Expected: (.*)\n(.*)")
		matches = pattern.search(x)
		inp = matches.group(1)
		expOut = matches.group(3)
		recOut = ''
		result = 'FAILED'
	else:
		pattern = re.compile("Input: (.*)\n(.*)Expected: (.*)\n(.*)Received: (.*)\n(.*)Result  :(.*)!")
		matches = pattern.search(x)
		inp = matches.group(1)
		expOut = matches.group(3)
		recOut = matches.group(5)
		result = matches.group(7)
	return inp, expOut, recOut, result

def checkResultTopcoder(failed = False):
	receivedPath = home + '/tools/tmp/tout'
	f = open(receivedPath)
	x = strClean(f.read())
	try:
		inp, expOut, recOut, result = getTestDataTopcoder(x, failed)
	except Exception as e:
		print e
		print x
		exit(0)
	#print inp, expOut, recOut, result
	inp = strClean(inp, forTopCoder = True) + '\n'
	expOut = strClean(expOut, forTopCoder = True) + '\n'
	recOut = strClean(recOut, forTopCoder = True) + '\n'
	result = strClean(result, forTopCoder = True)
	if(result == 'PASSED'):
		return inp, expOut, recOut, 'OK', ''
	else:
		return inp, expOut, recOut, 'WA', ''
	return x, 'WA', ''
	
def runCode(pathToExex, inputPath, outputPath, timeLimit, argument = ''):
	startTime = time.time()
	command = Command(pathToExex + ' ' + str(argument), inputPath)
	ret = command.run(timeout = timeLimit)	
	endTime = time.time()
	if(ret == 123456):
		return '', 'TLE', endTime - startTime, ''
	elif(ret == 0):
		output, verdict, msg = checkResult(outputPath)
		return output, verdict, endTime - startTime, msg
	else:
		output = cleanRTE(inputPath, pathToExex)
		return output, 'RTE', endTime - startTime, ''
	return None, None, endTime - startTime, ''

def runCodeTopcoder(pathToExex, inputPath, outputPath, timeLimit, argument = '', testId = 0):
	startTime = time.time()
	argument = str(testId)
	command = Command(pathToExex + ' ' + str(argument), inputPath)
	ret = command.run(timeout = timeLimit)	
	endTime = time.time()
	if(ret == 123456):
		inp, expOut, recOut, verdict, msg = checkResultTopcoder(failed = True)
		return inp, expOut, '', 'TLE', endTime - startTime, ''
	elif(ret == 0):
		inp, expOut, recOut, verdict, msg = checkResultTopcoder(failed = False)
		return inp, expOut, recOut, verdict, endTime - startTime, msg
	else:
		inp, expOut, recOut, verdict, msg = checkResultTopcoder(failed = True)
		output = cleanRTE(inputPath, pathToExex, isTopcoder = True, testCase = str(argument))
		return inp, expOut, output, 'RTE', endTime - startTime, msg
	return None, None, endTime - startTime, ''

def parseTopCoderGreed(sourcePath):
	f = open(sourcePath)
	x = f.read()
	f.close()
	if('Input' in x):
		return
	sys.stdout.write("Parsing tests... ")
	x = x.split('\n')
	nextto = ''
	for cur in x:
		if('do_test' in cur):
			nextto = cur
			i = 0
			while(i < len(cur) and cur[i] != '('):
				i += 1
				j = len(cur) - 1;
				while(j >= 0 and cur[j] != ','):
					j -= 1
			res = ''
			i += 1
			while(i < j):
				res += cur[i]
				i += 1
			res = res.split(',')
			variables = []
			for curVar in res:
				s = ''
				k = len(curVar) - 1
				while(curVar[k] != ' '):
					s = curVar[k] + s
					k -= 1
				variables.append(s)
			#print variables
			break
	f = open(sourcePath)
	x = f.read()
	f.close()
	s = nextto + '\n'
	s += 'cout << endl << "           Input: ";\n'
	for i in variables:
		if(i != variables[-1]):
			s += 'cout << to_string(' + i + ') << "<EOT>";\n'
		else:
			s += 'cout << to_string(' + i + ') << endl;\n'
	s = s.replace('\n', '\n    ')
	x = x.replace(nextto, s)
	f = open(sourcePath, 'w')
	f.write(x)
	f.close()
	sys.stdout.write('OK\n')

def getTotalTestsTopcoder(sourcePath):
	f = open(sourcePath)
	x = f.read()
	f.close()
	return x.count('-- Example')