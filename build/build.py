#!/usr/bin/python

import sys
import os
import re
import subprocess
import check
from utils import *
import config

#os.system('ls') #Clearning terminal

#Globals
home = os.path.expanduser("~")
fBaseName = sys.argv[1].replace('_test', '')
fileDir = sys.argv[2]
isTopcoder = False

if('topcoder' in sys.argv[2]):
    isTopcoder = True
    srcDir = sys.argv[2]
    mainDir = srcDir + '/main/'
    testsDir = mainDir
    #print srcDir
    #print mainDir
    #print testsDir
    os.system('cd %s && make demo ' % srcDir)
    exit(0)
    if(not os.path.isfile(srcDir + '/Makefile')):
        os.system('cp "%s/../Makefile" "%s/"' % (srcDir, srcDir))
    parseTopCoderGreed("%s%s_main.cpp" % (testsDir, fBaseName))
    #exit(0)
else:
    srcDir = '/home/chaitanya/cpro/src/'
    mainDir = '/home/chaitanya/cpro/main/'
    testsDir = '/home/chaitanya/cpro/tests/' + fBaseName + '/'
stressTest = False
warningsGenerated = None
if('_test' in sys.argv[1]):
    stressTest = True

def parseSource(fBaseName):
    #print bcolors.HEADER + "Generating source...",
    #fBaseName = sys.argv[1]
    sourceFName = "/home/chaitanya/cpro/src/%s.cpp" % fBaseName
    source = read(sourceFName)

    sourceMainF = "/home/chaitanya/cpro/main/%s_main.cpp" % fBaseName
    sourceMain = read(sourceMainF)

    utilsF = "/home/chaitanya/cpro/utils/utils.h"
    utils = read(utilsF)

    source = source.replace('#include "../utils/utils.h"', utils)

    sourceMain = sourceMain.replace('#include "../src/%s.cpp"' % fBaseName, source)
    sourceMain = re.sub('\n\n+', '\n\n', sourceMain)
    #sourceMain += '\n\n// SHelper plug-in\n'

    f = open('/home/chaitanya/cpro/output/main.cpp', 'w')
    f.write(sourceMain)
    f.close()
    #print "OK" + bcolors.ENDC

def compileSource(fileBase, destination):
    print bcolors.HEADER + "Compiling %s... " % (fileBase),
    sys.stdout.flush()
    sourceFName = "%s%s.cpp" % (srcDir, fileBase)
    sourceMainF = "%s%s_main.cpp" % (mainDir, fileBase)
    if(not isTopcoder):
        cmd = 'make PNAME="%s" DEST="%s" --directory=/home/chaitanya/cpro/' % (fileBase, destination)
    else:
        cmd = 'make PNAME="%s" DEST="%s" --directory="%s"' % (fileBase, destination, srcDir)
    #print
    #print cmd
    #exit(0)
    p = subprocess.Popen(cmd, bufsize = 4096, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    out, error = p.communicate()
    return out, error, p.returncode

out, err, retcode = compileSource(fBaseName, '/tmp/%s_a.out' % (fBaseName))
sys.stdout.flush()
if(str(retcode) != str(0)):
    print 'Failed' + bcolors.ENDC
    print bcolors.FAIL + '\n' + err + bcolors.ENDC
    exit(0)
print 'OK' + bcolors.ENDC
if(err != ''):
    print 'Compiler Warnings:'
    print '\033[93m' + '\n' + err + '\033[0m'
    warningsGenerated = True
else:
    print 'Compiler Warnings: None'

if(stressTest):
    out, err, retcode = compileSource(sys.argv[1], '/tmp/%s_stress.out' % (fBaseName))
    sys.stdout.flush()
    if(str(retcode) != '0'):
        print 'Failed' + bcolors.ENDC
        print bcolors.FAIL + '\n' + err + bcolors.ENDC
        exit(0)
    print 'OK' + bcolors.ENDC

print bcolors.HEADER + 'Running tests... '
sys.stdout.flush()

checker = check.checker('/tmp/%s_a.out' % (fBaseName), testsDir, fBaseName, isTopcoder)
#args: (path to executable, tests dir, source base name)

if(stressTest):
    result, verdicts = checker.stressTest('/tmp/%s_stress.out' % fBaseName)
else:
    result, verdicts = checker.check()

print "=================================================================="
if(result == True):
    maxTime = 0
    for i in verdicts:
        maxTime = max(maxTime, i[1])
    print "Test result:%s All tests passed in %.4f s. %s" % (bcolors.OKGREEN, maxTime, bcolors.ENDC)
elif(result == None):
    print 'Test result:%s No tests found. %s' % (bcolors.WARNING, bcolors.ENDC)
else:
    print "Test result:%s Failed %s\n" % (bcolors.FAIL, bcolors.ENDC)
    for testid, i in enumerate(verdicts):
        print "Test #%d: %s in %.4f s." % (testid + 1, i[0], i[1])

print
if(stressTest == False and not isTopcoder):
    parseSource(sys.argv[1].replace('_test', ''))
#print 'DONE'
if(warningsGenerated):
    print "Some warnings were generated during compilation."