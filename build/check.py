#!/usr/bin/python

import sys
import os
import glob
import json
import random
from utils import *
import config

#Globals
STRESSTESTS = config.STRESS_TESTS
TIMELIMIT = config.TIME_LIMIT
ADDTOMAINTESTS = config.ADD_TO_MAIN_TESTS
STOPONFAIL = config.STOP_ON_FAIL
INITIALSEED = config.INITIAL_SEED
if(INITIALSEED != None):
    random.seed(INITIALSEED)

class testConfig:
    knowAnswer = None
    skip = False
    def __init__(self):
        pass
    
class checker:
    pathToExec = None
    testsDir = None
    srcName = None
    isTopcoder = False
    testsConf = {}

    def __init__(self, pathToExec, testsDir, srcName, isTopcoder):
        self.pathToExec = pathToExec
        self.testsDir = testsDir
        self.srcName = srcName
        self.isTopcoder = isTopcoder
        sys.stdout.flush()

    def readTest(self, testid):
        inp = strClean(read(self.testsDir + '%d.in' % (testid)))
        out = strClean(read(self.testsDir + '%d.out' % (testid)))
        try:
            if(self.testsConf[testid].skip):
                return inp, "SKIPPED"
            elif(self.testsConf[testid].knowAnswer == False or out == '\n'):
                return inp, 'NULL'
        except KeyError:
            self.testsConf[testid] = testConfig()
            self.testsConf[testid].skip = False
            self.testsConf[testid].knowAnswer = True
        return inp, out

    def parseTestsConfig(self, confPath):
        res = {}
        try:
            with open(confPath) as f:
                conf = json.load(f)
        except Exception:
            for i in xrange(16): #Just 16 tests (0-15)
                res[i] = testConfig()
                res[i].knowAnswer = True
            return res
        for i in conf:
            cur = testConfig()
            if(conf[i]["skip"]):
                cur.skip = True
            else:
                cur.skip = False
            if(conf[i]["knowAnswer"]):
                cur.knowAnswer = True
            else:
                cur.knowAnswer = False
            res[int(i)] = cur
        return res

    def addToMainTests(self, inputPath, outputPath, testid):
        if(ADDTOMAINTESTS):
            os.system('cp %s %s' % (inputPath, self.testsDir + '%d.in' % testid))
            os.system('cp %s %s' % (outputPath, self.testsDir + '%d.out' % testid))
        return
    
    def check(self):
        if(self.isTopcoder):
            return self.checkTopcoder()
        self.testsConf = self.parseTestsConfig(self.testsDir + '/.tests.json')
        totalTests = len(glob.glob(self.testsDir + '*'))
        assert(totalTests % 2 == 0)
        totalTests /= 2
        result = None
        verdicts = []
        print "------------------------------------------------------------------"
        for testid in range(1, totalTests + 1):
            inputPath = self.testsDir + '%d.in' % (testid)
            outputPath = self.testsDir + '%d.out' % (testid)
            inp, out = self.readTest(testid)
            print "Test #%d:" % (testid),
            if(out == 'SKIPPED'):
                print "SKIPPED"
                verdicts.append(['SKIPPED', 0])
            else:
                print "\nInput:\n%s\nExpected output:\n%s\nExecution output:" % (truncate(inp), truncate(out))
                receivedOutput, curResult, runTime, msg = runCode(self.pathToExec, inputPath, outputPath, TIMELIMIT, '')
                if(curResult == 'OK' or curResult == "Unknown" or self.testsConf[testid].knowAnswer == False):
                    if(result == None or result == True):
                        result = True
                    if(not self.testsConf[testid].knowAnswer):
                        curResult = 'UNK'
                        msg = ''
                else:
                    result = False
                print truncate(receivedOutput)
                if(msg != ''):
                    print "Verdict: %s(%s) in %.4f s." % (curResult, msg, runTime)
                else:
                    print "Verdict: %s in %.4f s." % (curResult, runTime)
                verdicts.append([curResult, runTime])
            print "------------------------------------------------------------------"
            sys.stdout.flush()
            if(not result and STOPONFAIL):
                #print "HERE"
                #print "Result:", curResult
                break
        return result, verdicts
    
    def stressTest(self, stressPath):
        if(self.isTopcoder):
            return self.stressTestTopcoder(stressPath)
        result = None
        verdicts = []
        totalTests = len(glob.glob(self.testsDir + '*'))
        assert(totalTests % 2 == 0)
        totalTests /= 2
        for testid in range(1, STRESSTESTS + 1):
            inputPath = '/tmp/stressInput.txt'
            outputPath = '/tmp/stressOutput.txt'
            seed = random.randint(1, 10000)
            #TODO: Run the stress.out within timelimit
            os.system('%s %d %s %s' % (stressPath, seed, inputPath, outputPath))
            inp = strClean(read(inputPath))
            out = strClean(read(outputPath))
            print "Test #%d:" % (testid),
            print "\nInput:\n%s\nExpected output:\n%s\nExecution output:" % (truncate(inp), truncate(out))
            receivedOutput, curResult, runTime, msg = runCode(self.pathToExec, inputPath, outputPath, TIMELIMIT, '')
            if(curResult == 'OK' or curResult == 'Unknown'):
                if(result == None or result == True):
                    result = True
            else:
                totalTests += 1
                self.addToMainTests(inputPath, outputPath, totalTests)
                result = False
            print truncate(receivedOutput)
            print "Verdict: %s in %.4f s." % (curResult, runTime)
            verdicts.append([curResult, runTime])
            print "------------------------------------------------------------------"
            sys.stdout.flush()
            if(not result and STOPONFAIL):
                break
        return result, verdicts
    
    def checkTopcoder(self):
        #print self.testsDir
        #print self.srcName
        #os.system(self.pathToExec)
        os.system('touch /tmp/tempIn')
        os.system('touch /tmp/tempOut')
        self.testsConf = self.parseTestsConfig("%s.%s.tests.json" % (self.testsDir, self.srcName))
        totalTests = getTotalTestsTopcoder("%s%s.sample" % (self.testsDir, self.srcName))
        result = None
        verdicts = []
        print "------------------------------------------------------------------"
        for testid in range(0, totalTests):
            inputPath = '/tmp/tempIn'
            outputPath = '/tmp/tempOut'
            inp, out = '', ''#self.readTest(testid)
            print "Test #%d:" % (testid),
            if(out == 'SKIPPED'):
                print "SKIPPED"
                verdicts.append(['SKIPPED', 0])
            else:
                inp, out, receivedOutput, curResult, runTime, msg = runCodeTopcoder(self.pathToExec, inputPath, outputPath, TIMELIMIT, '', testid)
                inp = inp.replace('<EOT>', '\n')
                print "\nInput:\n%s\nExpected output:\n%s\nExecution output:" % (truncate(inp), truncate(out))
                if(curResult == 'OK' or curResult == "Unknown" or self.testsConf[testid].knowAnswer == False):
                    if(result == None or result == True):
                        result = True
                    if(not self.testsConf[testid].knowAnswer):
                        curResult = 'UNK'
                        msg = ''
                else:
                    result = False
                print truncate(receivedOutput)
                if(msg != ''):
                    print "Verdict: %s(%s) in %.4f s." % (curResult, msg, runTime)
                else:
                    print "Verdict: %s in %.4f s." % (curResult, runTime)
                verdicts.append([curResult, runTime])
            print "------------------------------------------------------------------"
            sys.stdout.flush()
            if(not result and STOPONFAIL):
                break
        return result, verdicts
    
    def stressTestTopcoder(self):
        return None, []