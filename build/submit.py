#!/usr/bin/python

import sys
import os
import re
import subprocess
import check
from utils import *
import config
import requests
import urllib
import json

home = os.path.expanduser("~")
fBaseName = sys.argv[1].replace('_test', '')
fileDir = sys.argv[2]
isTopcoder = False

def loadUrl(url, parse_json = True, depth = 0, use_proxy = True):
    if use_proxy == False:
        proxies = {
            "http": None,
            "https": None,
        }
    else:
        proxies = {
            'http': 'http://proxy.iiit.ac.in:8080',
            'https': 'http://proxy.iiit.ac.in:8080'
        }
    try:
        if(parse_json):
            response = requests.get(url, proxies=proxies).json()
        else:
            response = requests.get(url, proxies = proxies)
            return (True, response)
        return (True, response)
    except KeyError:
        return (False, 'Error')

print 'Submitting problem: ' + fBaseName

if('topcoder' in sys.argv[2]):
    isTopcoder = True
    srcDir = sys.argv[2]
    f = open(srcDir + '/../../.%s.json' % fBaseName)
    x = f.read()
    f.close()
    probleMetaData = json.loads(x)
    problemId = probleMetaData['problemId']
    outputFile = srcDir + '/' + fBaseName + '.cpp'
    
    url = 'http://127.0.0.1:8080/submit?oj=TopCoder&problemId=%s&fname=%s' % (problemId, urllib.quote_plus(outputFile))
    response = loadUrl(url, parse_json = False, use_proxy = False)[1]
    url = 'http://127.0.0.1:8080/status?problemId=%s' % (problemId)
    while True:
        response = loadUrl(url, parse_json = True, use_proxy = False)[1]
        if response['done'] == True:
            sys.stdout.write('[vjudge] %s          \n' % response['msg'])
            break
        else:
            sys.stdout.write('[vjudge] %s          \r' % response['msg'])
        sys.stdout.flush()
        time.sleep(0.5)

elif 'CF' == fBaseName[:2]:
    srcDir = '/home/chaitanya/cpro/src/'
    outputFile = '/home/chaitanya/cpro/output/main.cpp'
    problemId = fBaseName[2:]
    sys.stdout.write('[vjudge] Submitting problem          \r')
    url = 'http://127.0.0.1:8080/submit?oj=CodeForces&problemId=%s&fname=%s' % (problemId, urllib.quote_plus(outputFile))
    response = loadUrl(url, parse_json = False, use_proxy = False)[1]
    url = 'http://127.0.0.1:8080/status?problemId=%s' % (problemId)
    while True:
        response = loadUrl(url, parse_json = True, use_proxy = False)[1]
        if response['done'] == True:
            sys.stdout.write('[vjudge] %s          \n' % response['msg'])
            break
        else:
            sys.stdout.write('[vjudge] %s          \r' % response['msg'])
        sys.stdout.flush()
        time.sleep(0.5)
else:
    srcDir = '/home/chaitanya/cpro/src/'
    outputFile = '/home/chaitanya/cpro/output/main.cpp'
    print fBaseName
    print 'Not implemented for other judges'
