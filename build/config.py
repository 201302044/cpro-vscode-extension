#!/usr/bin/python

STRESS_TESTS = 10
STOP_ON_FAIL = False
IGNORE_PE = False
TIME_LIMIT = 1
PRINT_LIMIT = 1000
ADD_TO_MAIN_TESTS = False
INITIAL_SEED = None#Some number or None (None for no initial seed)
INTERACTIVE_TASK = False #TODO
COMPARE_AS_STRING = False #TODO | to compare the output which contains spaces in it. | If true, need to reformat output as well (currently stripping spaces at the end)
#FIXME