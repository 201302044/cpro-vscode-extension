from flask import Flask, jsonify, render_template, request
import subprocess
import os
import pprint
import json
import utils
import threading

app = Flask(__name__)

"""
@app.route('/_add_numbers')
def add_numbers():
   a = request.args.get('a', 0, type=int)
   b = request.args.get('b', 0, type=int)
   return jsonify(result=a + b)
"""

@app.route('/parseTask', methods=['POST'])
def parseTask():
   f = open('./tmp.html', 'w')
   f.write(request.data)
   f.close()
   os.system('python plugin.py')
   return "Ok"

@app.route('/config')
def configurations():
   return render_template('configurations.html')

@app.route('/submit', methods = ['POST', 'GET'])
def submit():
    oj = request.args.get('oj')
    problemId = request.args.get('problemId')
    fname = request.args.get('fname')
    t = threading.Thread(target=utils.vjudge.submitProblem, args=(oj, problemId, fname, ))
    t.start()
    return 'OK'

@app.route('/status', methods = ['POST', 'GET'])
def status():
    problemId = request.args.get('problemId')
    return jsonify(utils.cache[problemId])

@app.route('/', methods=['POST', 'GET'])
def index():
    data = json.loads(request.data)
    if('topcoder' in data['url']):
        problem_id = None
        problem_name = data['name'].replace('Problem Statement for ', '')
        problem_name = problem_name.lstrip().rstrip()
        url = data['url']
        for i in url.split('&'):
            if('pm=' in i):
                problem_id = i.replace('pm=', '')
        utils.saveDataTopcoder(problem_id, problem_name)
    elif('codeforces' in data['url']):
        url = data['url'].replace('http://', '')
        url = url.replace('https://', '')
        url = url.replace('www.codeforces.com/contest/', '')
        url = url.replace('codeforces.com/contest/', '')
        url = url.replace('problem/', '')
        url = url.replace('/', '')
        data['ProblemId'] = url[-1]
        data['ContestId'] = url[:-1]
        utils.saveData(data)
    else:
        print data['url']
        print data
        utils.saveData(data)
    return 'True'

if __name__ == '__main__':
   from gevent.wsgi import WSGIServer
   http_server = WSGIServer(('', 8080), app)
   http_server.serve_forever()
   #app.run(threaded = True, debug = True)
