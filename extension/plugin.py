# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugin.ui'
#
# Created: Sat Jul 16 09:34:37 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
#import config
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from functools import partial
import glob
import os
from urllib2 import *
from sys import *
import BeautifulSoup
import subprocess
from os.path import expanduser
import re

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(300, 459)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 0, 301, 451))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(0, 0, 151, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(0, 100, 301, 31))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(0, 150, 111, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.lineEdit_2 = QtGui.QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QtCore.QRect(0, 180, 301, 33))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(0, 280, 91, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.lineEdit_3 = QtGui.QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QtCore.QRect(0, 310, 261, 33))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.toolButton = QtGui.QToolButton(self.frame)
        self.toolButton.setGeometry(QtCore.QRect(270, 310, 31, 31))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.lineEdit_4 = QtGui.QLineEdit(self.frame)
        self.lineEdit_4.setGeometry(QtCore.QRect(0, 30, 301, 33))
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(0, 70, 121, 21))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.comboBox = QtGui.QComboBox(self.frame)
        self.comboBox.setGeometry(QtCore.QRect(90, 230, 211, 29))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(0, 230, 101, 31))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(10, 390, 121, 41))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.frame)
        self.pushButton_2.setGeometry(QtCore.QRect(150, 390, 131, 41))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.checkBox = QtGui.QCheckBox(self.frame)
        self.checkBox.setGeometry(QtCore.QRect(10, 350, 131, 26))
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.checkBox_2 = QtGui.QCheckBox(self.frame)
        self.checkBox_2.setGeometry(QtCore.QRect(160, 350, 121, 26))
        self.checkBox_2.setObjectName(_fromUtf8("checkBox_2"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Task", None))
        self.label.setText(_translate("MainWindow", " Task name:", None))
        self.label_2.setText(_translate("MainWindow", " Contest name :", None))
        self.label_3.setText(_translate("MainWindow", " Template :", None))
        self.toolButton.setText(_translate("MainWindow", "...", None))
        self.label_4.setText(_translate("MainWindow", " Problem name :", None))
        self.label_5.setText(_translate("MainWindow", "  Language:", None))
        self.pushButton.setText(_translate("MainWindow", "OK", None))
        self.pushButton_2.setText(_translate("MainWindow", "Cancel", None))
        self.checkBox.setText(_translate("MainWindow", "Test Case", None))
        self.checkBox_2.setText(_translate("MainWindow", "Stress Test", None))
        self.comboBox.setItemText(0, _translate("MainWindow", "C++", None))
        self.comboBox.setItemText(1, _translate("MainWindow", "Python", None))
        self.comboBox.setItemText(2, _translate("MainWindow", "Java", None))



app = QtGui.QApplication(sys.argv)
ui = Ui_MainWindow()
taskName = ''
html = None
judge = None

def handleChange():
    cur = ui.comboBox.currentText()
    if(cur == 'C++'):
        ui.lineEdit_3.setText('~/tools/templates/template.cpp')
    elif(cur == 'Python'):
        ui.lineEdit_3.setText('~/tools/templates/template.py')
    elif(cur == 'Java'):
        ui.lineEdit_3.setText('~/tools/templates/template.java')
    pass

def cancel():
    QtGui.qApp.closeAllWindows()

def center(MainWindow):
    frameGm = MainWindow.frameGeometry()
    centerPoint = QtGui.QDesktopWidget().availableGeometry().center()
    frameGm.moveCenter(centerPoint)
    MainWindow.move(frameGm.topLeft())


def parseAtCoder():
    global html
    global taskName
    soup = BeautifulSoup.BeautifulSoup(html)

    H1 = soup.findAll("h1")
    contestName = H1[0].findAll(text = True)[0]
    contestName = contestName.replace('\n', '')
    contestName = re.sub(' +', ' ', contestName)
    contestName = contestName.strip(' ').strip()
    contestName = contestName[::-1]
    contestName = contestName.strip(' ').strip()
    contestName = contestName[::-1]

    H2 = soup.findAll("h2")
    problemName = H2[0].findAll(text = True)[0]
    problemName = problemName.replace('\n', '')
    problemName = re.sub(' +', ' ', problemName)
    problemName = problemName.strip(' ').strip()
    problemName = problemName[::-1]
    problemName = problemName.strip(' ').strip()
    problemName = problemName[::-1]
    
    ui.lineEdit.setText(problemName)
    ui.lineEdit_2.setText(contestName)
    x = problemName
    s = 'Task'
    for i in x:
        if(i == '-'):
            break
        else:
            s += i
    taskName = s
    taskName = taskName.replace('\n', '')
    taskName = re.sub(' +', '', taskName)
    taskName = taskName.strip(' ').strip()
    taskName = taskName[::-1]
    taskName = taskName.strip(' ').strip()
    taskName = taskName[::-1]
    ui.lineEdit_4.setText(taskName)
    try:
        f = open('/home/chaitanya/SublimeProjects/CodeForces/info/' + pname, 'r')
        x = f.read()
        f.close()
        x = x.split('\n')
    except Exception:
        x = []
    while(len(x) <= 2):
        x.append('None')
    if(x[0] != 'None'):
        ui.checkBox.setChecked(True)
    if(x[1] != 'None'):
        ui.checkBox_2.setChecked(True)

def parseCodeForces():
    global html
    global taskName
    soup = BeautifulSoup.BeautifulSoup(html)
    DIV = soup.findAll("div", { "class" : "title" })
    problemName = DIV[0].findAll(text = True)[0]
    try:
        CONTEST = soup.findAll("th", {'class' : 'left'})
        contestName = CONTEST[0].findAll(text = True)[0]
    except Exception:
        contestName = 'Google Code Jam'
    ui.lineEdit.setText(problemName)
    ui.lineEdit_2.setText(contestName)
    x = problemName
    s = 'Task'
    for i in x:
        if(i == '.'):
            break
        else:
            s += i
    taskName = s
    ui.lineEdit_4.setText(s)
    pname = problemName.replace(' ', '_')
    try:
        f = open('/home/chaitanya/SublimeProjects/CodeForces/info/' + pname, 'r')
        x = f.read()
        f.close()
        x = x.split('\n')
    except Exception:
        x = []
    while(len(x) <= 2):
        x.append('None')
    if(x[0] != 'None'):
        ui.checkBox.setChecked(True)
    if(x[1] != 'None'):
        ui.checkBox_2.setChecked(True)

def parseHackerCup():
    global html
    global taskName
    soup = BeautifulSoup.BeautifulSoup(html)
    #DIV = soup.findAll("div", { "class" : "title" })
    prefix = 'Problem'
    ids = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    actualId = 'A'#None
    for i in ids:
        if(prefix + ' ' + i + '.' in html):
            actualId = i
            break
    #assert(actualId != None)
    problemName = prefix + ' ' + actualId
    try:
        CONTEST = soup.findAll("h2", { "class" : "uiHeaderTitle" })
        contestName = CONTEST[0].findAll(text = True)[0]
    except Exception:
        contestName = 'Facebook Hacker Cup'
    ui.lineEdit.setText('A')
    ui.lineEdit_2.setText(contestName)
    taskName = 'Task' + actualId
    ui.lineEdit_4.setText(taskName)
    pname = problemName.replace(' ', '_')
    try:
        f = open('/home/chaitanya/SublimeProjects/HackerCup/info/' + pname, 'r')
        x = f.read()
        f.close()
        x = x.split('\n')
    except Exception:
        x = []
    while(len(x) <= 2):
        x.append('None')
    if(x[0] != 'None'):
        ui.checkBox.setChecked(True)
    if(x[1] != 'None'):
        ui.checkBox_2.setChecked(True)

def parseHackerRank():
    global html
    global taskName
    soup = BeautifulSoup.BeautifulSoup(html)
    
    tmp = soup.findAll("h2", { "class" : "hr_tour-challenge-name pull-left mlT" })
    problemName = tmp[0].findAll(text = True)[0]
    problemName = problemName.strip()
    problemName = problemName[::-1]
    problemName = problemName.strip()
    problemName = problemName[::-1]
    contestName = 'Hacker Rank'#CONTEST[0].findAll(text = True)[0]
    ui.lineEdit.setText(problemName)
    ui.lineEdit_2.setText(contestName)
    taskName = problemName.lower()
    taskName = re.sub(' +', ' ', taskName)
    taskName = taskName.replace(' ', '-')
    ui.lineEdit_4.setText(taskName)
    pname = problemName.replace(' ', '_')
    try:
        f = open('/home/chaitanya/SublimeProjects/CodeJam/info/' + pname, 'r')
        x = f.read()
        f.close()
        x = x.split('\n')
    except Exception:
        x = []
    while(len(x) <= 2):
        x.append('None')
    if(x[0] != 'None'):
        ui.checkBox.setChecked(True)
    if(x[1] != 'None'):
        ui.checkBox_2.setChecked(True)

def parseCodeJam():
    global html
    global taskName
    soup = BeautifulSoup.BeautifulSoup(html)
    #DIV = soup.findAll("div", { "class" : "title" })
    DIV = soup.findAll("div", { "class" : "problem-io-wrapper" })
    #print "---------------"
    #print DIV
    #print "---------------"
    prefix = 'Problem'
    ids = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    actualId = None
    for i in ids:
        if(prefix + ' ' + i + '.' in html):
            actualId = i
            break
    assert(actualId != None)
    problemName = prefix + ' ' + actualId

    CONTEST = soup.findAll("div", { "id" : "dsb-contest-title" })
    contestName = CONTEST[0].findAll(text = True)[0]
    ui.lineEdit.setText(problemName)
    ui.lineEdit_2.setText(contestName)
    taskName = 'Task' + actualId
    ui.lineEdit_4.setText(taskName)
    pname = problemName.replace(' ', '_')
    try:
        f = open('/home/chaitanya/SublimeProjects/CodeJam/info/' + pname, 'r')
        x = f.read()
        f.close()
        x = x.split('\n')
    except Exception:
        x = []
    while(len(x) <= 2):
        x.append('None')
    if(x[0] != 'None'):
        ui.checkBox.setChecked(True)
    if(x[1] != 'None'):
        ui.checkBox_2.setChecked(True)

def parsecurTest(cur):
    cur = str(cur)
    soup = BeautifulSoup.BeautifulSoup(cur)
    cur = soup.findAll('pre', text = True)

    #cur = cur.replace('<pre>', '')
    #cur = cur.replace('</pre>', '')
    #cur = cur.replace('&gt;','>')
    #cur = cur.replace('&lt;','<')
    #cur = cur.replace('&quot;','"')
    #cur = cur.replace('&amp;','&')
    #cur = cur.replace('<br />','\n')
    #cur = cur.replace('<br/>','\n')
    #cur = cur.replace('</ br>','\n')
    #cur = cur.replace('</br>','\n')
    #cur = cur.replace('<br>','\n')
    #cur = cur.replace('< br>','\n')
    #cur = cur.split('\n')
    res = ''
    for i in cur:
        x = i
        x = x.strip(' ').strip('').strip(' ')
        if(x == ''):
            continue
        else:
            if(res == ''):
                res += x
            else:
                res += '\n'
                res += x
    assert(res[-1] != '\n')
    res += '\n'
    return res

def parseTests(testsPath):
    global html
    global judge
    if(judge == 'atcoder'):
        soup = BeautifulSoup.BeautifulSoup(html)
        DIV = soup.findAll("div", { "class" : "part" })
        i = 0
        L = len(DIV)
        cur = 1
        while(i < L):
            tmp = DIV[i].findAll(text = True)
            s = ''
            for j in tmp:
                s += str(j)
            if('sample input' in s.lower()):
                f = open(testsPath + str(cur) + '.in', 'w')
                g = open(testsPath + str(cur) + '.out', 'w')                
                p = ''
                q = ''
                for j in DIV[i].findAll('pre')[0]:
                    p += j
                for j in DIV[i + 1].findAll('pre')[0]:
                    q += j
                #print "x : ", p
                #print "y : ", q
                x = parsecurTest(p)
                y = parsecurTest(q)
                #print "x : ", x
                #print "y : ", y
                f.write(x)
                g.write(y)
                f.close()
                g.close()
                i += 2
                cur += 1
            else:
                i += 1
        pass
    elif(judge == 'codeforces'):
        soup = BeautifulSoup.BeautifulSoup(html)   
        DIV = soup.findAll("div", { "class" : "sample-test" })
        PRE = DIV[0].findAll('pre')
        print "---------------"
        print len(PRE)
        print "---------------"
        for i in PRE:
            print i
            print "-------------------------"
        assert(len(PRE) % 2 == 0)
        L = len(PRE)
        cur = 1
        i = 0
        print "tests path : <" + testsPath + '>'
        if(testsPath[-1] != '/'):
            testsPath += '/'
        while(i < L):
            f = open(testsPath + str(cur) + '.in', 'w')
            g = open(testsPath + str(cur) + '.out', 'w')
            x = parsecurTest(PRE[i])
            y = parsecurTest(PRE[i + 1])
            f.write(x)
            g.write(y)
            f.close()
            g.close()
            i += 2
            cur += 1
    else:
        pass
    print "Done"

def _replace(fname, tname):
    f = open(fname)
    x = f.read()
    f.close()
    x = x.replace('%TaskName%', tname)
    f = open(fname, 'w')
    f.write(x)
    f.close()

def saveData():
    global html
    srcDir = '/home/chaitanya/cpro/src/'
    mainDir = '/home/chaitanya/cpro/main/'
    testsDir = '/home/chaitanya/cpro/tests/'
    global taskName
    #assert(taskName != '')
    problemName = ui.lineEdit.text()
    contestName = ui.lineEdit_2.text()
    problemName = problemName.replace(' ', '_')
    print '   [Problem Name] :', ui.lineEdit.text()
    print '   [Contest Name] :', ui.lineEdit_2.text()
    print '   [Task Name]    :', ui.lineEdit_4.text()
    if(taskName == ''):
        taskName = str(ui.lineEdit_4.text())
        if(taskName == ''):
            taskName = problemName
        #taskName = str(problemName)
    if(ui.checkBox_2.isChecked()):
        ui.checkBox.setChecked(True)
    #print infoDir + problemName, os.path.isfile(infoDir + problemName)
    if(False):
        pass
    else:
        os.system('rm -rf %s*%s*' % (srcDir, taskName))
        os.system('rm -rf %s*%s*' % (mainDir, taskName))
        
        x = str(ui.lineEdit_3.text())
        #print taskName
        if(x.endswith('.cpp')):
            os.system('cat /home/chaitanya/s-helper/templates/Task.cpp > %s.cpp' % (srcDir + taskName))
            _replace(srcDir + taskName + '.cpp', taskName)
            os.system('cat /home/chaitanya/s-helper/templates/Task_test.cpp > %s_test.cpp' % (srcDir + taskName))
            _replace(srcDir + taskName + '_test.cpp', taskName)

            os.system('cat /home/chaitanya/s-helper/templates/Task_main.cpp > %s_main.cpp' % (mainDir + taskName))
            _replace(mainDir + taskName + '_main.cpp', taskName)
            os.system('cat /home/chaitanya/s-helper/templates/Task_test_main.cpp > %s_test_main.cpp' % (mainDir + taskName))
            _replace(mainDir + taskName + '_test_main.cpp', taskName)

            #print 'code -r %s.cpp' % (srcDir + taskName)
            print '%s' % (os.system('pwd'))
            #exit(0)
            
            os.system('code -r %s.cpp' % (srcDir + taskName))

        elif(x.endswith('.py')):
            print "Not Implemented"
            assert(False)
            #os.system('cat /home/chaitanya/tools/templates/template.py > ' + baseDir + taskName + '.py')    
            #os.system('subl ' + baseDir + taskName + '.py')
    if(True):
        os.system('rm -rf ' + testsDir + taskName + '/')
        os.system('mkdir ' + testsDir + taskName + '/')
        os.system('touch ' + testsDir + taskName + '/.tests.json')
        parseTests(testsDir + taskName + '/')
        #f = open(infoDir + problemName, 'w')
        #f.write('None\n')
        #f.write('None\n')
        #f.close()
    exit(0)

def main(_html = None):
    global html
    global judge
    import sys
    try:
        f = open('./tmp.html')
        html = f.read()
        f.close()
    except Exception:
        html = ''
    #html = _html

    MainWindow = QtGui.QMainWindow()
    ui.setupUi(MainWindow)

    ui.comboBox.currentIndexChanged.connect(handleChange)
    ui.lineEdit_3.setText('~/tools/templates/template.cpp')
    if('codeforces' in html.lower()):
        print "CodeForces"
        judge = 'codeforces'
        parseCodeForces()
    elif('atcoder' in html.lower()):
        print "AtCoder"
        judge = 'atcoder'
        parseAtCoder()
    elif('codechef' in html.lower()):
        print "Code Chef"
        judge = 'codechef'
        #parseCodeChef()
    elif('hackerrank' in html.lower()):
        print 'Hacker Rank'
        judge = 'hackerrank'
        parseHackerRank()
    elif('codejam' in html.lower() and 'google' in html.lower()):
        print 'Google Code Jam'
        judge = 'gcodejam'
        parseCodeJam()
    elif('hackercup' in html.lower() and 'facebook' in html.lower()):
        print 'Facebook Hacker Cup'
        judge = 'hackercup'
        parseHackerCup()
    elif('hackerearth' in html.lower()):
        print 'Hacker Earth'
        judge = 'hackerearth'
    else:
        print 'Unknown'

    QObject.connect(ui.pushButton, SIGNAL("clicked()"), saveData)
    QObject.connect(ui.pushButton_2, SIGNAL("clicked()"), cancel)
    center(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

main()