import json
import os
import sys
import urllib
import requests
import time
import base64
import urllib2

cache = {}

class vjudgeProcessor:
    def __init__(self, uname, passwd):
        self._domain = r'https://vjudge.net'
        self._login_url = self._domain + r'/user/login'
        self._edit_url = self._domain + r'/contest/edit'
        self._create_url = self._domain + r'/contest/create'
        self._check_login_status = self._domain + r'/user/checkLogInStatus'
        self._find_problem_url = self._domain + r'/problem/findProblemSimple'
        self._submit_url = self._domain + r'/problem/submit'
        self._submit_poll_url = self._domain + r'/solution/data/'
        self.uname = uname
        self.passwd = passwd
        self._ss = requests.session()
        self._headers = dict(
            zip(
                ['user-agent', 'content-type'],
                [
                    'Mozilla/5.0 (X11; Linux x86_64) '
                    'AppleWebKit/537.36 (KHTML, like Gecko) '
                    'Chrome/60.0.3112.113 Safari/537.36',
                    'application/x-www-form-urlencoded; charset=UTF-8'
                ]
            )
        )
        self.login()
    
    def login(self):
        print '[vjudge] Logging in...'
        while not self._ss.post(self._check_login_status).json():
            self._ss.post(
                self._login_url,
                data=dict(
                    zip(
                        ['username', 'password'],
                        [self.uname, self.passwd]
                    )
                ),
                headers=self._headers
            )
            time.sleep(1)
        print '[vjudge] Login successful'
        
    def submitProblem(self, oj, problemId, fname):
        print '[vjudge] Submitting problem'
        cache[problemId] = {
            'done': False,
            'msg': 'Submitting problem\n'
        }
        f = open(fname)
        source = f.read()
        source = urllib2.quote(source)
        f.close()
        source = base64.b64encode(source)
        #print source
        lid = 42
        if oj == 'TopCoder':
            lid = 3
        res = self._ss.post(
            self._submit_url,
            data={
                'oj': oj,
                'probNum': problemId,
                'source': source,
                'share': 0,
                'language': lid,
                'captcha': ''
            },
            headers=self._headers
        ).json()
        try:
            runId = res['runId']
            print '[vjudge] Successfully submitted, RunId: %s' % str(runId)
            cache[problemId]['msg'] = 'Successfully submitted, RunId: %s\n' % str(runId)
        except KeyError:
            print '[vjudge] Following error occurred while submitting:', res['error']
            if('Crawling has not finished!' in res['error']):
                print '[vjudge] Crawling the problem and retrying'
                cache[problemId]['msg'] = 'Retrying\n'
                self._ss.post(
                    'https://vjudge.net/problem/%s-%s' % (oj, str(problemId)),
                    data={},
                    headers=self._headers
                )
                return self.submitProblem(oj, problemId, fname)
            if ('Please login to submit' in res['error']):
                self.login()
                return self.submitProblem(oj, problemId, fname)
            return False
        print '[vjudge] Running...'
        cache[problemId]['msg'] = 'Running...'
        while True:
            res = self._ss.post(
                self._submit_poll_url + str(runId),
                data={
                    'showCode': False
                },
                headers=self._headers
            ).json()
            cache[problemId]['msg'] = res['status'] + '    '
            print '[vjudge] > ' + res['status']
            if(res['status'] != 'Submitted' and res['status'] != 'Pending' and ('Running' not in res['status'])):
                cache[problemId]['done'] = True
                print '[vjudge] ' + res['status']
                return True
            time.sleep(1)
        return False

    def checkLogInStatus(self):
        return self._ss.post(self._check_login_status).json()
    
vjudge = vjudgeProcessor('achaitanyasai', 'nikithasai')

def getProblemName(problem_name, contest_name, pid, cid):
    if('codeforces' in contest_name.lower()):
        return 'CF' + cid + pid
        #return 'Task%s' % problem_name.split('.')[0]
    else:
        return problem_name

def _replace(fname, tname):
    f = open(fname)
    x = f.read()
    f.close()
    x = x.replace('%TaskName%', tname)
    f = open(fname, 'w')
    f.write(x)
    f.close()

def saveData(data):
    srcDir = '/home/chaitanya/cpro/src/'
    mainDir = '/home/chaitanya/cpro/main/'

    problem_name = data['name'].replace(' ', '_')

    contest_name = data['group'].replace(' ', '_')
    if 'ProblemId' not in data:
        data['ProblemId'] = ''
    if 'ContestId' not in data:
        data['ContestId'] = ''

    problem_name = getProblemName(problem_name, contest_name, data['ProblemId'], data['ContestId'])

    taskName = problem_name
    print taskName

    os.system('rm -rf /home/chaitanya/cpro/src/%s*' % problem_name)
    os.system('rm -rf /home/chaitanya/cpro/main/%s*' % problem_name)
    
    f = open('/home/chaitanya/cpro/src/.%s.json' % taskName, 'w')
    json.dump(data, f)
    f.close()

    os.system('cat /home/chaitanya/s-helper/templates/Task.cpp > %s.cpp' % (srcDir + taskName))
    _replace(srcDir + taskName + '.cpp', taskName)
    os.system('cat /home/chaitanya/s-helper/templates/Task_test.cpp > %s_test.cpp' % (srcDir + taskName))
    _replace(srcDir + taskName + '_test.cpp', taskName)

    os.system('cat /home/chaitanya/s-helper/templates/Task_main.cpp > %s_main.cpp' % (mainDir + taskName))
    _replace(mainDir + taskName + '_main.cpp', taskName)
    os.system('cat /home/chaitanya/s-helper/templates/Task_test_main.cpp > %s_test_main.cpp' % (mainDir + taskName))
    _replace(mainDir + taskName + '_test_main.cpp', taskName)
    
    os.system('rm -rf /home/chaitanya/cpro/tests/%s*' % problem_name)
    os.system('mkdir /home/chaitanya/cpro/tests/%s' % problem_name)
    
    for j, test in enumerate(data['tests']):
        f = open('/home/chaitanya/cpro/tests/%s/%d.in' % (problem_name, j + 1), 'w')
        f.write('%s' % test['input'])
        f.close()
        f = open('/home/chaitanya/cpro/tests/%s/%d.out' % (problem_name, j + 1), 'w')
        f.write('%s' % test['output'])
        f.close()
    
    os.system('code -r %s.cpp' % (srcDir + taskName))    

def saveDataTopcoder(problem_id, problem_name):
    print 'Extracting problem'
    srcDir = '/home/chaitanya/cpro/topcoder/'
    os.system('rm -rf /home/chaitanya/cpro/topcoder/%s' % problem_name)
    os.system('export http_proxy=http://proxy.iiit.ac.in:8080;export https_proxy=http://proxy.iiit.ac.in:8080; gettc %s' % problem_id)
    os.system('mv %s %s%s' % (problem_name, srcDir, problem_name))
    
    f = open('/home/chaitanya/cpro/topcoder/%s/.%s.json' % (problem_name, problem_name), 'w')
    data = {
        'problemId': problem_id
    }
    json.dump(data, f)
    f.close()

    os.system('code -r %s%s/solve/cpp/%s.cpp' % (srcDir, problem_name, problem_name))
    return True
