#!/usr/bin/env python
"""
Very simple HTTP server in python.
Usage::
    ./dummy-web-server.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import os

class S(BaseHTTPRequestHandler):

   def _set_headers(self):
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()

   def do_GET(self):
      self._set_headers()
      self.wfile.write("<html><body><h1>hi!</h1></body></html>")

   def do_HEAD(self):
      self._set_headers()

   def do_POST(self):
      self._set_headers()
      self.data_string = self.rfile.read(int(self.headers['Content-Length']))
      try:
         f = open('./tmp.html', 'w')
         f.write(self.data_string)
         f.close()
         print 'Got Post Request'
         os.system('python plugin.py')
      except Exception as e:
         print e
         pass
      self.wfile.write(self.data_string)
        
def run(server_class=HTTPServer, handler_class=S, port=4243):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
