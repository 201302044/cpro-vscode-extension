#include "../src/%TaskName%_test.cpp"

int main(int argc, char *argv[]){
    int seed = stoi(string(argv[1]));
    string inputPath = string(argv[2]);
    string outputPath = string(argv[3]);
    %TaskName%TestCase tc;
    ifstream ifile;
    ofstream ofile;
    { //create test case;
        ofile.open(inputPath);
        customIO io(ifile, ofile);
        tc.createTests(seed, io);
        ofile.close();    
    }
    { //solve stupid;
        ifile.open(inputPath);
        ofile.open(outputPath);
        customIO io1(ifile, ofile);
        tc.solveStupid(io1);
        ifile.close();
        ofile.close();    
    }
    return 0;
}
