#include "../src/%TaskName%.cpp"

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    cout << fixed;
    cout.precision(20);
    istream& in(std::cin);
    ostream& out(std::cout);
    customIO io(in, out);
    %TaskName% solver;
    solver.solve(io);
    return 0;
}
